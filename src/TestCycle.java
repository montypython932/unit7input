import java.io.File;
import java.io.FileNotFoundException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class TestCycle {

	public static void main(String[] args) throws Exception {
		
		File inputFile = new File("F:/Workspace/erererUnit7Input/src/input.txt");
		
		try {
			Scanner inputStream = new Scanner(inputFile);
			Cycle cyc1 = new Cycle();
			Cycle cyc2 = new Cycle((byte) inputStream.nextByte(), inputStream.nextInt());
			inputStream.close();
			System.out.println(cyc1.toString());
			System.out.println(cyc2.toString());
		} catch (FileNotFoundException e) {
			System.out.println("File not Found!");
		} catch (InputMismatchException e) {
			System.out.println("Wrong datatype! First line is a byte, second is an int!");
		}
	}
}